package xyz.nullex;

public class Main {

    public static void main(String[] args) {
        String hex = toHex("100111010000");
        System.out.println(hex);

        String binary = toBin("9d0");
        System.out.println(binary);
    }

    public static String toHex(String bin) {
        StringBuilder result = new StringBuilder();
        if (bin == null || bin.isEmpty()) {
            return result.toString();
        }

        int mod = bin.length() % 4;
        if (mod != 0) {
            for (int i = 0; i < (4 - mod); i++) {
                // append leading zeros to normalize length
                bin = "0".concat(bin);
            }
        }
        try {
            for (int i = bin.length() - 4; i >= 0; i -= 4) {
                String block = bin.substring(i, i + 4);
                String hex = convertBinToHex(block);
                result.append(hex);
            }
        } catch (IllegalArgumentException e) {
            // string contains non-binary characters
            return "";
        }

        return result.reverse().toString();
    }

    public static String toBin(String hex) {
        StringBuilder result = new StringBuilder();
        if (hex == null || hex.isEmpty()) {
            return result.toString();
        }

        try {
            String[] chars = hex.split("");
            for (String hexChar : chars) {
                String bin = convertHexToBin(hexChar);
                result.append(bin);
            }
        } catch (IllegalArgumentException e) {
            // string contains non-hexadecimal characters
            return "";
        }

        int indexOfFirstNonZeroByte = result.toString().indexOf('1');
        if (indexOfFirstNonZeroByte == -1) {
            // the binary string contains only zeros, so this is equals zero.
            return "0";
        } else {
            // normalize string, remove leading zeros.
            return result.substring(indexOfFirstNonZeroByte);
        }
    }

    protected static String convertHexToBin(String hex) {
        switch (hex) {
            case "0": return "0000";
            case "1": return "0001";
            case "2": return "0010";
            case "3": return "0011";
            case "4": return "0100";
            case "5": return "0101";
            case "6": return "0110";
            case "7": return "0111";
            case "8": return "1000";
            case "9": return "1001";
            case "a": return "1010";
            case "b": return "1011";
            case "c": return "1100";
            case "d": return "1101";
            case "e": return "1110";
            case "f": return "1111";
            default:
                throw new IllegalArgumentException("String must contains a hexadecimal value.");
        }
    }

    protected static String convertBinToHex(String bin) {
        switch (bin) {
            case "0000": return "0";
            case "0001": return "1";
            case "0010": return "2";
            case "0011": return "3";
            case "0100": return "4";
            case "0101": return "5";
            case "0110": return "6";
            case "0111": return "7";
            case "1000": return "8";
            case "1001": return "9";
            case "1010": return "a";
            case "1011": return "b";
            case "1100": return "c";
            case "1101": return "d";
            case "1110": return "e";
            case "1111": return "f";
            default:
                throw new IllegalArgumentException("String must contains a binary value.");
        }
    }
}
